<?php
/**
 * Created by PhpStorm.
 * User: russ
 * Date: 28.04.18
 * Time: 9:55
 */

/**
 * Longest Common Substring Problem
 * https://www.wikiwand.com/en/Longest_common_substring_problem
 * Complexity: O(len(s1)*len(s2)) ~ O(n^2)
 *
 * @param string $s1
 * @param string $s2
 * @return string
 */
function LCSubstr(string $s1, string $s2): string
{
    $lcs = [];
    $a1 = str_split($s1);
    $a2 = str_split($s2);
    $n1 = count($a1);
    $n2 = count($a2);
    $z = 0;
    $L = [];

    for ($i = 0; $i < $n1; $i++) {
        for ($j = 0; $j < $n2; $j++) {
            if ($s1[$i] === $s2[$j]) {
                if ($i === 0 || $j === 0) {
                    $L[$i][$j] = 1;
                } else {
                    $L[$i][$j] = $L[$i-1][$j-1] + 1;
                }
                if ($L[$i][$j] > $z) {
                    $z = $L[$i][$j];
                    $lcs[$z] = $i;
                }
            } else {
                $L[$i][$j] = 0;
            }
        }
    }

    $lcs_z = max(array_keys($lcs));
    $lcs_i = $lcs[$lcs_z];
    $lcs_s = substr($s1, $lcs_i - $lcs_z + 1, $lcs_z);

    return $lcs_s;
}

print_r(LCSubstr('Doors', 'Kangaroo'));
